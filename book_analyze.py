import re
import requests

# pdfminer 一括処理
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfparser import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfparser import PDFPage
from pdfminer.pdfdevice import PDFDevice
from pdfminer.converter import PDFPageAggregator
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.layout import LTTextBoxHorizontal

# Google翻訳のURL
url = 'https://translate.google.com/?hl=ja#en/ja/'

# 翻訳用ファイル情報
folder_path = './'
file_name = '1-4'  # ファイル名から拡張子(.pdf)を外したもの

# 文字列置換用のリスト
# tupple list: [('置換対象文字列', '置換後文字列'), ... ]
str_sub_list = [
    ('\n', ' '),  # '\n' -> ' '
    ('\.\s', '.\n'), # '. ' -> '.\n'
    ('-\s', ''), # '- ' -> ''
    ('Fig\.\n', 'Fig. '),  # 'Fig.\n' -> 'Fig. '
    ('Chap\.\n', 'Chap. '),  # 'Chap.\n' -> 'Chap. '
    ('i\.e\.\n', 'i.e. '),  # 'i.e.\n' -> 'i.e. '
    ('e\.g\.\n', 'e.g. '),  # 'e.g.\n' -> 'e.g. '
]

# Open a PDF file.
fp = open(folder_path+'pdf/'+file_name+'.pdf', 'rb')

# Create a PDF parser object associated with the file object.
parser = PDFParser(fp)
document = PDFDocument()
parser.set_document(document)

# Create a PDF document object that stores the document structure.
# Supply the password for initialization.
password=""
document.set_parser(parser)
document.initialize(password)

# Create a PDF resource manager object that stores shared resources.
rsrcmgr = PDFResourceManager()

# Set parameters for analysis.
laparams = LAParams()

# Create a PDF page aggregator object.
device = PDFPageAggregator(rsrcmgr, laparams=laparams)
interpreter = PDFPageInterpreter(rsrcmgr, device)

txt = ""  # テキストを格納
for page in list(document.get_pages()):
    # interpreter page
    interpreter.process_page(page)
    # receive the LTPage object for the page.
    # layoutの中にページを構成する要素（LTTextBoxHorizontalなど）が入っている
    layout = device.get_result()
    i = 0
    for l in layout:
        if isinstance(l, LTTextBoxHorizontal) and i > 2:
            t = l.get_text()  # テキストを取得
            m = re.match('[1-9][0-9]*\.[1-9]+', t)  # 見出しを検索
            if m:
                txt = txt + t + '.\n'
            else:
                txt = txt + t[:len(t)-1] + ' '  # 末尾の改行を空白に置換して追加
        i = i + 1  # 最初の3つを飛ばす

# str_sub_listに基づいて文字列置換
for t in str_sub_list:
    comp = re.compile(t[0])
    txt = comp.sub(t[1], txt)

# 整形したテキストを保存
f = open(folder_path+'txt/' + file_name +'.txt', 'w')
f.write(txt)
f.close()


### 以下翻訳 ###

# pdfから抽出したtxtを読み出す
src_txt = open(folder_path+'txt/' + file_name +'.txt')
lines = src_txt.readlines()
src_txt.close()

# Google翻訳に1文ずつ投げて翻訳する
texts = []
texts_translated = []
pattern = "TRANSLATED_TEXT=\'(.*?)\'"
for i, line in enumerate(lines, 1):
    print( "%.3f%% (%d/%d)" % (100.0*i/len(lines), i, len(lines)))
    text = line.rstrip('\r\n')
    try:
        r = requests.get(url, params={'q': text})
        result = re.search(pattern, r.text)
        if result:
            texts.append(text)
            texts_translated.append(result.group(1))
    except:
        # TODO: 例外処理
        print("Exception")
        texts.append("# Exception.")
        texts_translated.append("# Exception.")

# 元の文章と翻訳語の文章を.mdファイルに追加
f = open(folder_path+'result/'+file_name+'_translated.md', 'w')
for text, text_translated in zip(texts, texts_translated):
    f.write('> ' + text + '\n\n')
    f.write(text_translated + '\n\n')
f.close()

print('End')