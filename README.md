# 概要
Structured Computer Organization(第6版)を自動翻訳するためのスクリプト
# 使い方
1. Python3系の環境を準備する
2. モジュール "requests" をPytohn3環境にインストール
3. モジュール "pdfminer3k" をPython3環境にインストール(詳しくは["PythonでPDFを処理できるpdfminer3kの使い方メモ"](http://cartman0.hatenablog.com/entry/2017/08/26/022957))
4. pdf/ に翻訳するpdfファイルを配置する
5. book_analyze.py の変数 "file_name" に、3で配置したpdfのファイル名を書く( `pdf/chapter1.pdf` と配置した場合は `filename = "chapter1"` )
6. book_analyze.py を実行する

# オプション
## 文字列置換
book_analyze.pyのリスト "str_sub_list" に `('置換対象文字列', '置換後文字列')` の形式のタプルを追加すると、文字列の置換ができる